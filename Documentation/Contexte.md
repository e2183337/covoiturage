Le projet consiste à développer une application web permettant à une petite communauté d'une ville de partager des déplacements vers les villes voisines. Cette demande provient de résidents de la ville de Val-des-Lacs qui veulent se déplacer à St-Donat ou Ste-Agathe, mais l'application doit être conçue de façon à pouvoir être offerte à d'autres communautés par la suite.

**Le but de cette application est de:**

* Réduire les émissions de GES;
* Réduire les coûts liés à la consommation d'essence et l'entretien des véhicules ;
* Tisser des liens entre les membres d'une communauté.

## Fonctionnement général

L'application sert à mettre en contact des personnes qui veulent proposer des déplacements avec d'autres qui veulent se déplacer sans devoir utiliser leur voiture. Le nombre de villes accessibles est très restreint. Chaque passager paie un prix déterminé au conducteur. Le pris est fixé de façon à créer un incitatif chez les conducteurs potentiels sans décourager les passagers potentiels d'utiliser le service. L'application ne gère pas d'argent.