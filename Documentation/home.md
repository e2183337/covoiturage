# Projet de covoiturage

## [Contexte](Contexte)

## [Fonctionnalités](Fonctionnalit%C3%A9s)

## [Livrable 1](Livrable%201)

* [Modèle logique de données](Modèle logique)
* [CRUD](CRUD)
* [Couche d'accès aux données](Accès aux données)